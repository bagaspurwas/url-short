package main
import (
    "math"
    _ "github.com/go-sql-driver/mysql"
    "database/sql"
)

/*
Number <--> ASCII
0 -- '0'
1 -- '1'
.
10 -- A
11 -- B
.
35 -- a
36 -- b
.
61 -- z
*/

const ASCII_0 = 48
const ASCII_A = 65
const ASCII_a = 97

/*
url_map table
+-----+-----------+
| id  |  fullUrl  |
+-----+-----------+
|     |           |
|     |           |
*/

var DATABASE_HOST = getEnv("DATABASE_HOST", "localhost")
var DATABASE_NAME = getEnv("DATABASE_NAME", "url_short")
var DATABASE_USER = getEnv("DATABASE_USER", "root")
var DATABASE_PASS = getEnv("DATABASE_PASS", "example")

var dbUrl string = DATABASE_USER + ":" + DATABASE_PASS + "@tcp(" + DATABASE_HOST + ":3306)/" + DATABASE_NAME

func mapNumberToAscii(numbers []int32) string {
    var ascii string = ""
    for _, e := range numbers {
        if e < 10 {
            ascii = ascii + string(e + ASCII_0)
        } else if e < 36 {
            ascii = ascii + string(e + ASCII_A - 10)
        } else {
            ascii = ascii + string(e + ASCII_a - 36)
        }
    }
    return ascii
}

func mapAsciiToNumber(ascii string) []int32 {
    var numbers []int32
    for _, e := range ascii {
        if e < ASCII_0 + 10 {
            numbers = append(numbers, e - ASCII_0)
        } else if e < ASCII_A + 26 {
            numbers = append(numbers, e - ASCII_A + 10)
        } else {
            numbers = append(numbers, e - ASCII_a + 36)
        }
    }
    return numbers
}

func shortenedUrlToIndex(shortenedUrl string) int64 {
    var index int64 = 0
    urlNum := mapAsciiToNumber(shortenedUrl)
    /* Reverse the number sequence */
    for i, j := 0, len(urlNum)-1; i < j; i, j = i+1, j-1 {
        urlNum[i], urlNum[j] = urlNum[j], urlNum[i]
    }
    for i, e := range urlNum {
        index = index + int64(e)*int64(math.Pow(62,float64(i)))
    }

    return index
}

func indexToShortenedUrl(index int64) string {
    var base62num []int32
    var shortenedUrl string
    var remainder int32 = 0
    for index > 0 {
        remainder = int32(index % 62)
        base62num = append(base62num, remainder)
        index = index/62
    }
    for i, j := 0, len(base62num)-1; i < j; i, j = i+1, j-1 {
        base62num[i], base62num[j] = base62num[j], base62num[i]
    }
    shortenedUrl = mapNumberToAscii(base62num)
    return shortenedUrl
}

func prepareDatabase() {
    db, err := sql.Open("mysql", dbUrl)
    if err != nil {
        handleError(err)
    }
    defer db.Close()
    query := (`CREATE TABLE IF NOT EXISTS url_map(
             id INT NOT NULL AUTO_INCREMENT,
             fullUrl VARCHAR(2083) NOT NULL,
             PRIMARY KEY (id))`)

    stmt, err := db.Prepare(query)
    if err != nil {
        handleError(err)
    }
    defer stmt.Close()

    _, err = stmt.Exec()
    if err != nil {
        handleError(err)
    }
    defer stmt.Close()
}

func insertFullUrl(fullUrl string) (int64, error) {
    var index int64

    db, err := sql.Open("mysql", dbUrl)
    if err != nil {
        handleError(err)
    }
    defer db.Close() 

    lastIndexQuery := "SELECT MAX(id) FROM url_map"
    insertQuery := "INSERT INTO url_map (fullUrl) values (?)"
    lastIndexStatement, err := db.Prepare(lastIndexQuery)
    if err != nil {
        handleError(err)
    }
    defer lastIndexStatement.Close()

    err = lastIndexStatement.QueryRow().Scan(&index)
    if err != nil {
        handleError(err)
    }
    index = index + 1
    insertStatement, err := db.Prepare(insertQuery)
    if err != nil {
        handleError(err)
    }
    defer insertStatement.Close()

    _, err = insertStatement.Exec(fullUrl)
    if err != nil {
        handleError(err)
    }

    return index, err
}


func getFullUrl(shortenedUrl string) (string, error) {
    var query string
    var fullUrl string

    db, err := sql.Open("mysql", dbUrl)
    if err != nil {
        handleError(err)
    }
    defer db.Close()
    index := shortenedUrlToIndex(shortenedUrl)
    query = ("SELECT fullUrl FROM url_map WHERE id = ?")

    getStmt, err := db.Prepare(query)
    if err != nil {
        handleError(err)
    }
    defer getStmt.Close()

    err = getStmt.QueryRow(index).Scan(&fullUrl)
    if err != nil {
        handleError(err)
    }

    return fullUrl, err
}
