package main

import (
	"github.com/gorilla/mux"
	"net/http"
	"encoding/json"
	"time"
	"log"
)

var HOSTNAME = getEnv("HOSTNAME", "localhost")
var HTTP_AUTH_USER = getEnv("HTTP_AUTH_USER", "bapung")
var HTTP_AUTH_PASS = getEnv("HTTP_AUTH_PASS", "jambankuda")

func UrlRedirector(w http.ResponseWriter, r *http.Request) {
	log.Println(r.URL.Path)
	short := r.URL.Path[1:]
	fullUrl, err := getFullUrl(short)
	if err != nil {
		http.Error(w, "Internal Server Error", 500)
	}
	if fullUrl != "" {
		http.Redirect(w, r, fullUrl, 302)
	}	
}

func ShortenUrl(w http.ResponseWriter, r *http.Request) {
	/*
	POST
	Authorization: Basic YmFwdW5nOmphbWJhbmt1ZGE= //bapung:jambankuda
	*/
	type UrlRequestBody struct {
		Url string `json:"url"`
	}
	type UrlResponseBody struct {
		ShortUrl string `json:"shortUrl"`
	}

	var reqBody UrlRequestBody
	var resBody UrlResponseBody
	var reqUser, reqPass, reqOk = r.BasicAuth()

	if reqUser != HTTP_AUTH_USER || reqPass != HTTP_AUTH_PASS || !reqOk {
		http.Error(w, "Not Authorized", 401)
		return
	}
	if r.Body == nil {
		http.Error(w, "Request body is empty", 400)
		return
	}

	err := json.NewDecoder(r.Body).Decode(&reqBody)
	if err != nil {
		http.Error(w, "Request body is bad", 400)
	}
	if !IsValidUrl(reqBody.Url) {
		http.Error(w, "URL is badly formatted", 400)
		return
	}
	id, err := insertFullUrl(reqBody.Url)
	if err != nil {
		http.Error(w, "Internal Server Error", 500)
		return
	}
	resBody.ShortUrl = "http://" + HOSTNAME + "/" + indexToShortenedUrl(id)
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(resBody)
}


func main() {
	prepareDatabase()
	r := mux.NewRouter()
	server := r.Host(HOSTNAME).Subrouter()

	server.HandleFunc("/{[a-z]+[A-Z]+[0-9]+}", UrlRedirector).Methods("GET")
	server.HandleFunc("/shorten_url", ShortenUrl).Methods("POST")

	srv := &http.Server{
		Handler:	  r,
        Addr:         "0.0.0.0:8000",
        WriteTimeout: 5 * time.Second,
        ReadTimeout:  5 * time.Second,
	}
	log.Fatal(srv.ListenAndServe())
}