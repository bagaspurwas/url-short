package main

import (
    "testing"
    "fmt"
)

func TestMapNumberXAscii(t *testing.T) {
    var numberCase = []int32 {0,1,10,35,36,61}
    var stringCase string = "01AZaz"
    if mapNumberToAscii(numberCase) != stringCase {
        t.Errorf("Conversion of number to ASCII not comply the specification")
    }
    numberConvResult := mapAsciiToNumber(stringCase)
    for i, e := range numberConvResult {
        if numberCase[i] != e {
            t.Errorf("Conversion of ASCII to number not comply the specification")
        }
    }
}

func TestIndexToShortenedUrl(t *testing.T) {
    expectationTables := []struct {
        index int64
        expString string
    }{
        {1000, "G8"},
        {5000, "1Ie"},
    }
    for _, table := range expectationTables {
        outString := indexToShortenedUrl(table.index)
        if outString != table.expString {
            t.Errorf("Index %d results in string %s. Expected string: %s",
            table.index, outString, table.expString)
        }
    }
}

func TestShortenedUrlToIndex(t *testing.T) {
    expectationTables := []struct {
        inputString string
        expectedIndex int64
    }{
        {"G8", 1000},
        {"1Ie", 5000},
    }
    for _, table := range expectationTables {
        outIndex := shortenedUrlToIndex(table.inputString)
        if outIndex != table.expectedIndex {
            t.Errorf("String %s results in index %d. Expected index: %d",
            table.inputString, outIndex, table.expectedIndex)
        }
    }
}

func TestWriteAndReadUrl(t *testing.T) {
    var fullUrlTest string = "https://www.its.ac.id"
    index, err := insertFullUrl(fullUrlTest)
    handleError(err)
    shortenedUrl := indexToShortenedUrl(index)
    fmt.Println(shortenedUrl)

    var obtainedFullUrl string
    obtainedFullUrl, err = getFullUrl(shortenedUrl)
    handleError(err)

    if obtainedFullUrl != fullUrlTest {
        t.Errorf("Inserted URL and obtained full URL doesn't match")
    }
}