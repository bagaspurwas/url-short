FROM golang:1.14-alpine as builder

WORKDIR /go/src/app
COPY . .
RUN mkdir /user && \
    echo 'nobody:x:65534:65534:nobody:/:' > /user/passwd && \
    echo 'nobody:x:65534:' > /user/group
RUN apk update && apk add git
RUN go get -d -v ./...
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags '-extldflags "-static"' -o url-short .

FROM scratch as app

COPY --from=builder /user/group /user/passwd /etc/
COPY --from=builder /go/src/app/url-short /
WORKDIR /
ENTRYPOINT ["/url-short"]
USER nobody:nobody

EXPOSE 8000