package main

import (
	"net/url"
	"log"
	"os"
)

func handleError(e error) {
    log.Println(e)
}

func getEnv(key string, fallback string) string {
	value := os.Getenv(key)
	if len(value) == 0 {
	    return fallback
	}
	return value
}

func IsValidUrl(str string) bool {
	/* Supported url length < 2083 character */
	if len(str) > 2083 {
		return false
	}
    u, err := url.Parse(str)
    return err == nil && u.Scheme != "" && u.Host != ""
}